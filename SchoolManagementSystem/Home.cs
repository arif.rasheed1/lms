﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagementSystem.Registration;


namespace SchoolManagementSystem
{
    public partial class frmHome : Form
    {
        public frmHome()
        {
            InitializeComponent();
        }

        private void mnuItemStudentRegistration_Click(object sender, EventArgs e)
        {
            frmStudentRegistration _frmStudentRegistration = new frmStudentRegistration();
            _frmStudentRegistration.ShowDialog();
        }
    }
}
